#!/bin/bash
BUCKET=$(echo "$1" | tr '[:upper:]' '[:lower:]'); # ensure lowercase for valid bucket name
PREFIX="$2";

ERROR_COUNT=0;

echo "Validating AWS CloudFormation templates..."

# Loop through the YAML templates in this repository
keys=$(aws s3api list-objects-v2 --bucket $BUCKET --prefix $PREFIX --query "Contents[].Key" --output json | jq -r '.[]' | sed 's/ /\%20/g')
for key in ${keys[@]}; do 
    # Validate the template with CloudFormation
    if [[ $key =~ \.(template|json|yml|yaml)$ ]]; then
      {
        ERRORS=$((aws cloudformation validate-template --template-url https://s3-$REGION.amazonaws.com/$BUCKET/$key) 2>&1)
      } && {
        echo "[pass] $key";
      } || {
        ERROR_COUNT=$((ERROR_COUNT+1))
        echo "[fail] $key: $ERRORS";
      }
    fi
done;

echo "$ERROR_COUNT template validation error(s)"; 
if [ "$ERROR_COUNT" -gt 0 ];
    then exit 1; 
fi
