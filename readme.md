## CloudFormation Tests

Contains a bash script for performing CloudFormation tests.

**If this is your first time visiting this project, check jhctechnology/aws-cloudformation-utilities/awslint> first for an introduction and contribution guidelines.**

# Example

```
validate:
  stage: test
  script:
    - chmod a+x ./cfn-test/*.sh
    - ./cfn-test/lint-templates.sh
    - ./cfn-test/validate-templates.sh path/to/cloudformation/templates
```
# Requirements

Libraries required to be installed on the gitlab runner instance:
 * [yamllint](https://github.com/adrienverge/yamllint)
 * [jq](https://github.com/stedolan/jq)
